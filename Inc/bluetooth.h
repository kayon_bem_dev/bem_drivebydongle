#ifndef _bluetooth_h_
#define _bluetooth_h_
#include "stm32f0xx_hal.h"

/*******************************************************************************
 Prototypy funkcji
*******************************************************************************/

void BLE_FN(void);
void Rx_dtble(uint8_t Rx_dta);
void Send_ble(uint8_t tablica[]);
uint8_t Test_dtble(uint8_t tablica[]);

extern void Wait_ms(uint16_t Tim_ms);



/******************************************************************************/


#endif