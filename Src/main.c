/**
  ******************************************************************************
  * File Name          : main.c
  * Description        : Main program body
  ******************************************************************************
  * This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * Copyright (c) 2018 STMicroelectronics International N.V. 
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f0xx_hal.h"
#include "adc.h"
#include "i2c.h"
#include "iwdg.h"
#include "spi.h"
#include "usart.h"
#include "usb_device.h"
#include "gpio.h"

/* USER CODE BEGIN Includes */
#include "s2lp.h"
#include "bluetooth.h"

/* USER CODE END Includes */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/
volatile uint8_t FLAGS0 =0; 
volatile uint16_t Wt_ms;
volatile uint16_t Licz_rmk;
volatile uint8_t Tim_ld3 =0;
volatile uint8_t Tim_1s =0;
volatile uint8_t Tim_ld2 =0;
volatile uint8_t Adc_bat =0;
volatile uint16_t Tim_lv =9000;
volatile uint8_t Tim_bt = 80;
volatile uint8_t Cnt_awr =0;
extern uint8_t WMB_Idbuf[300] __attribute__((aligned(4)));
extern volatile uint8_t Il_wmb;
extern volatile uint8_t Ind_RxIrqBLE;
extern uint8_t BufIdlBLE[255];
uint8_t TmP_buf[16] __attribute__((aligned(4)));

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);

/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/
void Wait_ms(uint16_t Tim_ms);
void Test_swon(void);
void Test_swoff(void);
void Akt_sg(void);	
void Msr_vbt(void);		
void Test_Toff(void);	
extern uint8_t CDC_Transmit_FS(uint8_t* Buf, uint16_t Len);

/* USER CODE END PFP */

/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

int main(void)
{

  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration----------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_ADC_Init();
  MX_I2C1_Init();
	#ifndef DEBUG
  MX_IWDG_Init();
	#endif
  MX_SPI1_Init();
  MX_USART1_UART_Init();
  MX_USB_DEVICE_Init();

  /* USER CODE BEGIN 2 */
	HAL_ADC_MspInit(&hadc);
	HAL_ADCEx_Calibration_Start(&hadc);
	Test_swon();
	HAL_UART_MspInit(&huart1);		
  HAL_SPI_MspInit(&hspi1);	
	
	while((Read_Spr(0x8E) >> 1) != 0x30)				
	 {		
		S2LP_INIT(); 	  
		S2LP_RX();	 
	 }	 

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
  /* USER CODE END WHILE */

  /* USER CODE BEGIN 3 */
	 BLE_FN();			
	 if(FLAGS0 & MLDP_akt)	
	  {  
		 HAL_GPIO_WritePin(LED2_GPIO_Port, LED2_Pin, GPIO_PIN_RESET);			
		 Tim_lv = 9000;	
		}	
	 else	
	  {
		 if(Tim_ld2 != 0)
		  {	
			 HAL_GPIO_WritePin(LED2_GPIO_Port, LED2_Pin, GPIO_PIN_RESET);			
			}	
		 else	
		  {  			
		   HAL_GPIO_WritePin(LED2_GPIO_Port, LED2_Pin, GPIO_PIN_SET);				
			}					
		}	
						
	 if(FLAGS0 & mn100ms)
	  {  
		 FLAGS0 &= ~mn100ms;		 	 		 							
		 #ifndef DEBUG	
		 HAL_IWDG_Refresh(&hiwdg);		 		 							 		 
		 #endif					
		 if(Tim_ld2 != 0)
		  { 
			 Tim_ld2--;			
		  }		
		 
		 Test_swoff();						 					 			
		 Akt_sg();
		 Msr_vbt();		
		 Test_Toff();	
		}
		
	 if(FLAGS0 & Dt_wmb)
	  {		 
		 FLAGS0 &= ~Dt_wmb;				 		 
		 Tim_ld3 = 20;
		 Tim_lv = 9000;		
		 CDC_Transmit_FS(WMB_Idbuf,Il_wmb); 			 			  		
		 if(FLAGS0 & MLDP_akt)
		  {		
			 HAL_UART_Transmit_IT(&huart1, WMB_Idbuf, Il_wmb);			 												 
			}	   
	  }
	 else
	  { 	
		 if(FLAGS0 & Ms_bt)
		  {  
			 FLAGS0 &= ~Ms_bt;
			 #ifdef Frm0xAA
			 TmP_buf[0] = 0xAA;	
			 TmP_buf[1] = 0xAA;		
			 TmP_buf[2] = 0xAA;		
			 TmP_buf[3] = 0xAA;		
			 TmP_buf[4] = Adc_bat;
			 TmP_buf[5] = Wer_mj;		
			 TmP_buf[6] = Wer_mi;			
			 TmP_buf[7] = Wer_rel;			
			 TmP_buf[8] = TmP_buf[0] + TmP_buf[1] + TmP_buf[2] + TmP_buf[3] + TmP_buf[4] + TmP_buf[5] + TmP_buf[6] + TmP_buf[7]; 	
			 CDC_Transmit_FS(TmP_buf,9); 			 			  			
			 if(FLAGS0 & MLDP_akt)
		    {		
			   HAL_UART_Transmit_IT(&huart1, TmP_buf, 9);			 												 
			  }	   					
			 #endif			
		  }		
		}			
  }	
  /* USER CODE END 3 */

}

/** System Clock Configuration
*/
void SystemClock_Config(void)
{

  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;
  RCC_PeriphCLKInitTypeDef PeriphClkInit;

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI|RCC_OSCILLATORTYPE_HSI14
                              |RCC_OSCILLATORTYPE_LSI|RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSI14State = RCC_HSI14_ON;
  RCC_OscInitStruct.HSICalibrationValue = 16;
  RCC_OscInitStruct.HSI14CalibrationValue = 16;
  RCC_OscInitStruct.LSIState = RCC_LSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL6;
  RCC_OscInitStruct.PLL.PREDIV = RCC_PREDIV_DIV1;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_1) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USB|RCC_PERIPHCLK_USART1
                              |RCC_PERIPHCLK_I2C1;
  PeriphClkInit.Usart1ClockSelection = RCC_USART1CLKSOURCE_PCLK1;
  PeriphClkInit.I2c1ClockSelection = RCC_I2C1CLKSOURCE_HSI;
  PeriphClkInit.UsbClockSelection = RCC_USBCLKSOURCE_PLL;

  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure the Systick interrupt time 
    */
  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

    /**Configure the Systick 
    */
  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 3, 0);
}

/* USER CODE BEGIN 4 */
/*******************************************************************************
 Funkcja opoznienia czasowego ms
*******************************************************************************/

void Wait_ms(uint16_t Tim_ms)
 {	
  Wt_ms = Tim_ms;	 
  while(Wt_ms != 0){}
 }	 
 
/*******************************************************************************
 Test zalaczenia zasilania
*******************************************************************************/
 
void Test_swon(void)
 {
  GPIO_InitTypeDef GPIO_InitStruct; 
	volatile uint8_t i=0;
	 
	while(1)
	 {		
	  while((FLAGS0 & mn100ms) == 0){}			 	 			 	
	  FLAGS0 &= ~mn100ms;		 	 		 	  	
		#ifndef DEBUG	
		 HAL_IWDG_Refresh(&hiwdg);		 		 							 		 
		 #endif					
	  if(HAL_GPIO_ReadPin(KL_GPIO_Port, KL_Pin))
	   {				
			HAL_GPIO_TogglePin(LED3_GPIO_Port, LED3_Pin); 
			HAL_GPIO_TogglePin(LED2_GPIO_Port, LED2_Pin);  			 
		  i++; 		 
	    if(i > 5)
		   {	
				GPIO_InitStruct.Pin = SETU_Pin;
				GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
				GPIO_InitStruct.Pull = GPIO_NOPULL;
				GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
				HAL_GPIO_Init(SETU_GPIO_Port, &GPIO_InitStruct);						
				HAL_GPIO_WritePin(SETU_GPIO_Port, SETU_Pin, GPIO_PIN_SET);	 
		    HAL_GPIO_WritePin(LED3_GPIO_Port, LED3_Pin, GPIO_PIN_RESET);
				HAL_GPIO_WritePin(LED2_GPIO_Port, LED2_Pin, GPIO_PIN_RESET);
				while(HAL_GPIO_ReadPin(KL_GPIO_Port, KL_Pin))
				 {					 
				  #ifndef DEBUG	
		      HAL_IWDG_Refresh(&hiwdg);		 		 							 		 
		      #endif					 				 
				 }	 
				Wait_ms(1000);
				HAL_GPIO_WritePin(LED3_GPIO_Port, LED3_Pin, GPIO_PIN_SET);
				HAL_GPIO_WritePin(LED2_GPIO_Port, LED2_Pin, GPIO_PIN_SET); 				 
				break; 
		   }	 
	   } 
   }		
 }	

/*******************************************************************************
 Test wylaczenia zasilania
*******************************************************************************/

void Test_swoff(void)
 {
	GPIO_InitTypeDef GPIO_InitStruct; 
	uint8_t i=0;
	 
	while(1)
	 {		
	  while((FLAGS0 & mn100ms) == 0){}			 
	  FLAGS0 &= ~mn100ms;		 	 		 	  		
		#ifndef DEBUG	
		 HAL_IWDG_Refresh(&hiwdg);		 		 							 		 
		 #endif					
	  if(HAL_GPIO_ReadPin(KL_GPIO_Port, KL_Pin))
	   {				
			HAL_GPIO_WritePin(LED3_GPIO_Port, LED3_Pin, GPIO_PIN_RESET);
			HAL_GPIO_WritePin(LED2_GPIO_Port, LED2_Pin, GPIO_PIN_RESET);
		  i++; 		 
	    if(i > 5)
		   {				
				GPIO_InitStruct.Pin = SETU_Pin;
				GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
        GPIO_InitStruct.Pull = GPIO_NOPULL;
				HAL_GPIO_Init(SETU_GPIO_Port, &GPIO_InitStruct);									
				HAL_GPIO_WritePin(LED3_GPIO_Port, LED3_Pin, GPIO_PIN_SET);
				HAL_GPIO_WritePin(LED2_GPIO_Port, LED2_Pin, GPIO_PIN_SET);				
				while(1)
				 {	
					#ifndef DEBUG	
					HAL_IWDG_Refresh(&hiwdg);		 		 							 		 
					#endif					
				 }	 				
		   }	 
	   }
		else
		 {
			HAL_GPIO_WritePin(LED3_GPIO_Port, LED3_Pin, GPIO_PIN_SET);
			HAL_GPIO_WritePin(LED2_GPIO_Port, LED2_Pin, GPIO_PIN_SET); 
		  break; 
		 }	 			
   }		
 }	

/*******************************************************************************
 Sygnalizacja aktywnosci
*******************************************************************************/ 
 
void Akt_sg(void)
 {
  if(Tim_1s < 5)
	 {		
	  Tim_1s++;
	 }	 
	else	 
	 { 
	  Tim_1s =0;
		if((FLAGS0 & MLDP_akt) == 0)
		 {		
		  Tim_ld2 =1;
  	 }	 
	 }	
 }	
 
/*******************************************************************************
 Pomiar napiecia baterii
*******************************************************************************/ 
 
void Msr_vbt(void)
 {
	GPIO_InitTypeDef GPIO_InitStruct;  
	 
	if(Tim_bt < 99)
	 {			
		Tim_bt++;
	 }			
	else
	 {				
		Tim_bt =0;				
	  HAL_ADC_Start(&hadc);   
	  while ((ADC1->ISR & ADC_ISR_EOC) == 0){} 	 			 
	  Adc_bat = (uint8_t) ((66 * ADC1->DR) >> 12);
		FLAGS0 |= Ms_bt;	 
			 
		if(Adc_bat < 33)	 
		 {  
		  if(Cnt_awr < 10)
			 {  
			  Cnt_awr++;
			 }		
			else
			 {			
				HAL_GPIO_WritePin(LED3_GPIO_Port, LED3_Pin, GPIO_PIN_RESET);
				HAL_GPIO_WritePin(LED2_GPIO_Port, LED2_Pin, GPIO_PIN_RESET); 
				Wait_ms(1000);
				HAL_GPIO_WritePin(LED3_GPIO_Port, LED3_Pin, GPIO_PIN_SET);
				HAL_GPIO_WritePin(LED2_GPIO_Port, LED2_Pin, GPIO_PIN_SET);				 
				GPIO_InitStruct.Pin = SETU_Pin;
				GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
				GPIO_InitStruct.Pull = GPIO_NOPULL;
				HAL_GPIO_Init(SETU_GPIO_Port, &GPIO_InitStruct);											
				while(1){}		 					
			 }	 
		 }
		else	
		 {		
			Cnt_awr =0;
		 }
	 }		 
}			 
			 
/*******************************************************************************
 Test autowylaczenia 10min bez aktywnosci
*******************************************************************************/	
	
void Test_Toff(void)
 {
	GPIO_InitTypeDef GPIO_InitStruct; 
	 
  if(Tim_lv != 0)
	 {	
		Tim_lv--;
	 }	 
	else
	 {		
		HAL_GPIO_WritePin(LED3_GPIO_Port, LED3_Pin, GPIO_PIN_RESET);
		HAL_GPIO_WritePin(LED2_GPIO_Port, LED2_Pin, GPIO_PIN_RESET); 
		Wait_ms(1000);
		HAL_GPIO_WritePin(LED3_GPIO_Port, LED3_Pin, GPIO_PIN_SET);
		HAL_GPIO_WritePin(LED2_GPIO_Port, LED2_Pin, GPIO_PIN_SET);				 
    GPIO_InitStruct.Pin = SETU_Pin;
	  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
		HAL_GPIO_Init(SETU_GPIO_Port, &GPIO_InitStruct);											
		while(1){}		 				
   } 		 
 }	
	

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  None
  * @retval None
  */
void _Error_Handler(char * file, int line)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  while(1) 
  {
  }
  /* USER CODE END Error_Handler_Debug */ 
}

#ifdef USE_FULL_ASSERT

/**
   * @brief Reports the name of the source file and the source line number
   * where the assert_param error has occurred.
   * @param file: pointer to the source file name
   * @param line: assert_param error line source number
   * @retval None
   */
void assert_failed(uint8_t* file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
    ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */

}

#endif

/**
  * @}
  */ 

/**
  * @}
*/ 

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
