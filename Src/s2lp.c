#include "stm32f0xx_hal.h"
#include "s2lp.h"
#include "spi.h"

/*******************************************************************************
 Definicje zmiennych
*******************************************************************************/

uint8_t SPITxBuf[8] __attribute__((aligned(4)));
uint8_t SPIRxBuf[8] __attribute__((aligned(4)));
 
/*******************************************************************************
 Konfiguracja ukladu S2LP
*******************************************************************************/

void S2LP_INIT(void)
 {	 	  
  HAL_GPIO_WritePin(SDN_GPIO_Port, SDN_Pin, GPIO_PIN_SET);
	Wait_ms(20);
  HAL_GPIO_WritePin(SDN_GPIO_Port, SDN_Pin, GPIO_PIN_RESET);	 
  Wait_ms(20);	 
	SPITxBuf[0] = Sp_Cmd;
	SPITxBuf[1] = Sp_Rst; 
	Write_spi(SPITxBuf, 2);	 
	Wait_ms(10); 
	SPITxBuf[0] = Sp_Cmd;
	SPITxBuf[1] = Sp_Stb; 
	Write_spi(SPITxBuf, 2);	  
	Wait_ms(10); 
		
	SPITxBuf[0] = Sp_Wr;
	SPITxBuf[1] = 0x09; 
	SPITxBuf[2] = 0x29;    
	Write_spi(SPITxBuf, 3);    
	 	
	SPITxBuf[0] = Sp_Wr;
	SPITxBuf[1] = 0x0A; 
	SPITxBuf[2] = 0xB7;    
	Write_spi(SPITxBuf, 3);     
	  	 	
	SPITxBuf[0] = Sp_Wr;
	SPITxBuf[1] = 0x0E; 
	SPITxBuf[2] = 0xF8;    
	Write_spi(SPITxBuf, 3); 
	 	 
	SPITxBuf[0] = Sp_Wr;
	SPITxBuf[1] = 0x0F; 
	SPITxBuf[2] = 0x20;    
	Write_spi(SPITxBuf, 3);

	SPITxBuf[0] = Sp_Wr;
	SPITxBuf[1] = 0x10; 
	SPITxBuf[2] = 0x08;    
	Write_spi(SPITxBuf, 3);

	SPITxBuf[0] = Sp_Wr;
	SPITxBuf[1] = 0x11; 
	SPITxBuf[2] = 0x04;    
	Write_spi(SPITxBuf, 3);

	SPITxBuf[0] = Sp_Wr;
	SPITxBuf[1] = 0x12; 
	SPITxBuf[2] = 0xF8;    
	Write_spi(SPITxBuf, 3);
		 	
	SPITxBuf[0] = Sp_Wr;
	SPITxBuf[1] = 0x13; 
	SPITxBuf[2] = 0x81;	
	Write_spi(SPITxBuf, 3);
	
	SPITxBuf[0] = Sp_Wr;
	SPITxBuf[1] = 0x05; 
	SPITxBuf[2] = (uint8_t)(F868_950 >> 24);    
	Write_spi(SPITxBuf, 3);
	
	SPITxBuf[0] = Sp_Wr;
	SPITxBuf[1] = 0x06; 
	SPITxBuf[2] = (uint8_t)(F868_950 >> 16);    
	Write_spi(SPITxBuf, 3);
	
	SPITxBuf[0] = Sp_Wr;
	SPITxBuf[1] = 0x07; 
	SPITxBuf[2] = (uint8_t)(F868_950 >> 8);    
	Write_spi(SPITxBuf, 3);
	
	SPITxBuf[0] = Sp_Wr;
	SPITxBuf[1] = 0x08; 
	SPITxBuf[2] = (uint8_t)(F868_950);    
	Write_spi(SPITxBuf, 3);

	SPITxBuf[0] = Sp_Wr;
	SPITxBuf[1] = 0x1F; 
	SPITxBuf[2] = 0x55;    
	Write_spi(SPITxBuf, 3);
	 	 
	SPITxBuf[0] = Sp_Wr;
	SPITxBuf[1] = 0x2E; 
	SPITxBuf[2] = 0x20;    
	Write_spi(SPITxBuf, 3);
	
	SPITxBuf[0] = Sp_Wr;
	SPITxBuf[1] = 0x30; 
	SPITxBuf[2] = 0x08;    
	Write_spi(SPITxBuf, 3);
			
	SPITxBuf[0] = Sp_Wr;
	SPITxBuf[1] = 0x5A; 
	SPITxBuf[2] = 0x01;    
	Write_spi(SPITxBuf, 3);
	
	SPITxBuf[0] = Sp_Wr;
	SPITxBuf[1] = 0x62; 
	SPITxBuf[2] = 0x47;    
	Write_spi(SPITxBuf, 3);
	
	SPITxBuf[0] = Sp_Wr;
	SPITxBuf[1] = 0x75; 
	SPITxBuf[2] = 0x37;    
	Write_spi(SPITxBuf, 3);
	
	SPITxBuf[0] = Sp_Cmd;
	SPITxBuf[1] = Sp_Rdy; 
	Write_spi(SPITxBuf, 2);	
	Wait_ms(10);
 } 
	  
/*******************************************************************************
 RX dla S2LP
*******************************************************************************/
 
void S2LP_RX(void)
 {	
  SPITxBuf[0] = Sp_Wr;																	// konfiguracja port�w S2LP do RX	(GPIO_0	CLK_RX, GPIO_1 DATA_RX)
  SPITxBuf[1] = 0x02; 
	SPITxBuf[2] = 0x42;    
	Write_spi(SPITxBuf,3);    
	 
	SPITxBuf[0] = Sp_Wr;																
  SPITxBuf[1] = 0x03; 
	SPITxBuf[2] = 0x4A;    
	Write_spi(SPITxBuf,3);    
	 
	SPITxBuf[0] = Sp_Cmd;
	SPITxBuf[1] = Sp_Rx; 
	Write_spi(SPITxBuf,2);  	
	 
	GPIO_InitTypeDef GPIO_InitStruct;  
  GPIO_InitStruct.Pin = DATA_S2_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;  
	GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(DATA_S2_GPIO_Port, &GPIO_InitStruct);  	
	Wait_ms(10);
 }
 
/*******************************************************************************
 Odczyt bajtu danych z S2LP
*******************************************************************************/			

uint8_t Read_Spr(uint8_t Adr_spr)
 {
  SPITxBuf[0] = Sp_Rd;
	SPITxBuf[1] = Adr_spr; 
	SPITxBuf[2] = 0xFF; 
  HAL_GPIO_WritePin(CSN_GPIO_Port, CSN_Pin, GPIO_PIN_RESET);					 
  HAL_SPI_TransmitReceive(&hspi1, SPITxBuf, SPIRxBuf, 3, 5);	
	HAL_GPIO_WritePin(CSN_GPIO_Port, CSN_Pin, GPIO_PIN_SET);						 
	return SPIRxBuf[2]; 
 }
  
/*******************************************************************************
 Zapis, odczyt do, z SPI
*******************************************************************************/ 

void Write_spi(uint8_t *Spi_bf, uint16_t Size)	
 {
	volatile uint8_t i =0;      
	for(i=0; i<200; i++) {}
  HAL_GPIO_WritePin(CSN_GPIO_Port, CSN_Pin, GPIO_PIN_RESET);					
	HAL_SPI_Transmit(&hspi1,Spi_bf, Size, 5); 		 		
  HAL_GPIO_WritePin(CSN_GPIO_Port, CSN_Pin, GPIO_PIN_SET);						 
 }
 
