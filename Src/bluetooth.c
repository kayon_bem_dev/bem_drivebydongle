#include "stm32f0xx_hal.h"
#include "bluetooth.h"
#include "usart.h"

/*******************************************************************************
 Definicje zmiennych
*******************************************************************************/

extern volatile uint8_t FLAGS0; 
uint8_t BufIrqBLE[64];
uint8_t BufIdlBLE[64];
uint8_t BufTxBLE[64];

volatile uint8_t Ind_RxIrqBLE =0;
volatile uint8_t Ind_RxIdlBLE =0;
volatile uint8_t Stan_ble =0;
volatile uint16_t Tim_ble =0;

/******************************************************************************/

uint8_t Tab_rst[] =  {'s','f',',','1',10,0};
uint8_t Tab_aok[] =  {'A','O','K',0};
uint8_t Tab_sn[] =   {'s','n',',','K','a','y','o','n','D','b','D',10,0};											 
uint8_t Tab_sr[] =   {'s','r',',','3','2','0','0','0','0','0','0',10,0};
uint8_t Tab_rbt[] =  {'r',',','1',10,0};
uint8_t Tab_con[] =  {'C','o','n','n','e','c','t','e','d',0};
uint8_t Tab_disc[] = {'C','o','n','n','e','c','t','i','o','n',' ','E','n','d',0};

uint8_t Tab_I[]    = {'I',0x0A};
uint8_t Tab_mld[]  = {'M','L','D','P',0};

/*******************************************************************************
 Konfiguracja ukladu RN4020
*******************************************************************************/

void BLE_FN(void)
 {
	if(Test_dtble(Tab_con))							  // czy pojawilo sie connected
	 {	 
	  Stan_ble = 10;
		Tim_ble = 500; 
	 }	 
	 	 
	if(Test_dtble(Tab_disc))							// czy pojawilo sie diosconnected
	 {
		if(Stan_ble >= 3)
		 {			 
	    Stan_ble = 0;		
		 }	 
	 }	  
	 	 
	switch(Stan_ble)
	 {	
/******************************************************************************/	  
		 
	  case 0x00:	 												// reset modulu						
		if(Tim_ble == 0)
		 {							
			FLAGS0 &= ~MLDP_akt;  		  
	    HAL_GPIO_WritePin(WAKE_SW_GPIO_Port, WAKE_SW_Pin, GPIO_PIN_RESET);
	    HAL_GPIO_WritePin(WAKE_HW_GPIO_Port, WAKE_HW_Pin, GPIO_PIN_RESET); 
		  Tim_ble = 100;
		  Stan_ble =1;
		 }	 
		break;
		
/******************************************************************************/	  		
		
		case 0x01:	  
		if(Tim_ble == 0)
		 {			
	    HAL_GPIO_WritePin(WAKE_SW_GPIO_Port, WAKE_SW_Pin, GPIO_PIN_SET);
	    HAL_GPIO_WritePin(WAKE_HW_GPIO_Port, WAKE_HW_Pin, GPIO_PIN_SET); 
	    Tim_ble = 100;
		  Stan_ble =2;
		 }	 
		break;
		
/******************************************************************************/	  				
		
		case 0x02:	  
		if(Tim_ble == 0)
		 {					
		  Send_ble(Tab_rst);									// sf,1,0x0A
		  Tim_ble = 500;
	    Stan_ble =3;
		 }	 
		break;
		
/******************************************************************************/		
		
		case 0x03:			
		if(Test_dtble(Tab_aok))							// oczekiwanie na potwierdzenie resetu			
		 {	
		  Tim_ble = 500;			 
	    Stan_ble =4;
		 }
		else
		 {		
		  if(Tim_ble == 0)
			 {	
				Stan_ble =0;
			 }	 
		 }		
		break;  
		 
/******************************************************************************/			 
		 
		case 0x04:						 							// nadanie nazwy 
			
		if(Tim_ble == 0)
		 {				
		  Send_ble(Tab_sn);									// sn,DL-WMB,0x0A			 
		  Tim_ble = 500;
	    Stan_ble =5;
		 }	 			 
		break;
		 
/******************************************************************************/			 

    case 0x05:				
		if(Test_dtble(Tab_aok))							// oczekiwanie na potwierdzenie nadania nazwy
		 {	
		  Tim_ble = 500;
	    Stan_ble =6;
		 }
		else
		 {		
		  if(Tim_ble == 0)
			 {	
				Stan_ble =0;
			 }	 
		 }		
		break;  

/******************************************************************************/			 

		case 0x06:						 							// konfiguracja modulu
		if(Tim_ble == 0)
		 {				
		  Send_ble(Tab_sr);									// sr,32000000,0x0A			 
		  Tim_ble = 500;
	    Stan_ble =7;
		 }	 			 
		break;

/******************************************************************************/			 		 

		case 0x07:				
		if(Test_dtble(Tab_aok))							// oczekiwanie na potwierdzenie konfiguracji
		 {	
		  Tim_ble = 500;
	    Stan_ble =8;
		 }
		else
		 {		
		  if(Tim_ble == 0)
			 {	
				Stan_ble =0;
			 }	 
		 }		
		break;  
		 
/******************************************************************************/			 		 
		 
		case 0x08:						 							  // restart modulu
		if(Tim_ble == 0)
		 {				
		  Send_ble(Tab_rbt);									// r,1,0x0A			 			 		  
	    Stan_ble =9;
		 }	 			 
		break; 
		 
/******************************************************************************/			 		 		 
		 
		 case 0x09:						 							  // modul skonfigurowany, oczekiwanie na polaczenie lub przesylanie danych w mldp		 
		 break; 
		 
/******************************************************************************/			 		 		 		 
		 
		 case 0x0A:														// polecenie wejscia w mldp	
	   if(Tim_ble == 0)
		  {						 
		   Send_ble(Tab_I);									  // I,0x0A			 			 		  
	     Stan_ble =0x0B;
			 Tim_ble = 2000;	
		  }	 			 			
		 break; 
		 
/******************************************************************************/			 		 		 		 		 
		 
		case 0x0B:	
		if(Test_dtble(Tab_mld))							// oczekiwanie na potwierdzenie wejscia w mldp
		 {			  
	    Stan_ble =9;			
			FLAGS0 |= MLDP_akt; 			
		 }
		else
		 {		
		  if(Tim_ble == 0)
			 {	
				Stan_ble =0;
			 }	 
		 }					
		break; 
		
/******************************************************************************/			 		 		 		 		 		
	 }		 
 }		 
 		 
/*******************************************************************************
 Funkcja odbioru danych z RN4020
*******************************************************************************/
 
void Rx_dtble(uint8_t Rx_dta)
 {
	uint16_t i=0; 
	 
  if((Rx_dta < 0x20) || (Ind_RxIrqBLE >= 64))
	 { 
		if(Ind_RxIrqBLE != 0)
		 {
			for(i=0; i<64; i++)
			 { 
			  BufIdlBLE[i] = BufIrqBLE[i];
				BufIrqBLE[i] =0; 				 
			 }	 
			Ind_RxIdlBLE = Ind_RxIrqBLE;
			Ind_RxIrqBLE =0; 			 
		  FLAGS0 |= Rx_ble;			 						
		 }		 
	 } 		 
	else
	 {		
		BufIrqBLE[Ind_RxIrqBLE] = Rx_dta; 
		Ind_RxIrqBLE++; 
	 }	 
 }		 

/*******************************************************************************
 Funkcja wysylania komend at do RN4020
*******************************************************************************/ 
 
void Send_ble(uint8_t tablica[])
 {
	uint8_t i=0;

  for(i=0; i<64; i++)
	 {
		if(tablica[i] != 0)
		 {		
      BufTxBLE[i] = tablica[i]; 
		 }
		else
		 {		
			break;
		 }		
	 }
	FLAGS0 &= ~Rx_ble; 
  HAL_UART_Transmit_IT(&huart1, BufTxBLE, i);			 				 	
 }	 
	
/*******************************************************************************
 Funkcja testujaca odebrany ciag
*******************************************************************************/ 
 
uint8_t Test_dtble(uint8_t tablica[])
 {
	uint8_t i=0; 
  if(FLAGS0 & Rx_ble)
	 {	
		for(i=0; i<64; i++)
		 {
			if(tablica[i] == 0)
			 {	
				return 1;
			 }	 
			if(BufIdlBLE[i] !=  tablica[i])
			 {		
			  return 0;
			 }	 
		 }  
	 }
  return 0; 	 
 } 
 
 